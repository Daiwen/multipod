/// <reference types="node" />
/// <reference types="jquery" />
/// <reference types="react" />

declare function AppLayout(props);

function markAsRead(ids : string[], callback : () => void) {
   var xmlhttp = new XMLHttpRequest();
   xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
         callback();
      }
   };
   var params = "?ids=" + ids;
   xmlhttp.open("PUT", "@{ReadEpisodesR}" + params);
   xmlhttp.send(null);
}

function removePlaylist(id : string, callback : () => void) {
   var xmlhttp = new XMLHttpRequest();
   xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
         callback();
      }
   };
   var params = "?id=" + id;
   xmlhttp.open("DELETE", "@{PlaylistR}" + params);
   xmlhttp.send(null);
}

function setCurrentTime(id: string, time: number) {
   var xmlhttp = new XMLHttpRequest();
   var params = "?id=" + id + "&time=" + time;
   xmlhttp.open("PUT", "@{SetCurrentTimeR}" + params);
   xmlhttp.send(null);
}

interface EpisodeData {
   id: string;
   title: string;
   url: string;
   isRead: boolean;
   isInPlaylist: boolean;
   currentTime: number;
}


interface EpisodeProperties {
   data: EpisodeData;
   removePlaylist: (string) => void;
   setCurrentEpisode: (EpisodeData) => void;
   isCurrentEpisode: boolean;
}

class Episode extends React.Component<EpisodeProperties, {}> {
   constructor(props) {
      super(props);

   }

   render() {
      const id = this.props.data.id;
      const title = this.props.data.title;
      const url = this.props.data.url;
      var episodeStyle = "episode";

      if (this.props.isCurrentEpisode) {
         episodeStyle = "current-episode " + episodeStyle;
      }

      let removePlaylist = "@{HomeR}/static/img/x-circle.svg";

      return (
         <div className={episodeStyle}
              onDoubleClick={() => this.props.setCurrentEpisode(this.props.data)}>
            {title}
            <input type="image" onClick={() => this.props.removePlaylist(this.props.data.id)} src={removePlaylist} />
         </div>
      );
   }
}


interface ReaderProperties {
   currentEpisode: EpisodeData;
   setCurrentTime: (string, number) => void;
   readOnEnd: (string) => void;
}

interface ReaderState {
   totalDuration: number;
   isDragging: boolean;
}

class Reader extends React.Component<ReaderProperties, ReaderState> {
   constructor(props) {
      super(props);
      this.state = { totalDuration: NaN,
                     isDragging: false};

      this.updateReaderTime = this.updateReaderTime.bind(this);
      this.applyReaderTime = this.applyReaderTime.bind(this);
      this.setDragging = this.setDragging.bind(this);
   }

   updateReaderTime() {
      if (!this.state.isDragging) {
         var audio = $("#podcast-reader")[0] as HTMLAudioElement;
         this.props.setCurrentTime(this.props.currentEpisode.id, Math.floor(audio.currentTime));
         this.setState({ totalDuration: Math.floor(audio.duration) });
      }
   }

   setDragging(event){
      this.props.setCurrentTime(this.props.currentEpisode.id, event.target.value);
      this.setState({ isDragging: true });
   }

   applyReaderTime(event) {
      var audio = $("#podcast-reader")[0] as HTMLAudioElement;
      this.props.setCurrentTime(this.props.currentEpisode.id, event.target.value);
      this.setState({ isDragging: false });
      audio.currentTime = event.target.value;
      setCurrentTime(this.props.currentEpisode.id, event.target.value);
   }

   componentDidMount() {
      if (this.props.currentEpisode === null)
         return;

      var audio = $("#podcast-reader")[0] as HTMLAudioElement;
      audio.currentTime = this.props.currentEpisode.currentTime;
   }

   render() {
      if (this.props.currentEpisode === null)
         return null;

      const id = this.props.currentEpisode.id;
      const url = this.props.currentEpisode.url;

      return (
         <div id="reader">
               {this.props.currentEpisode.title}
               <input type="range" value={this.props.currentEpisode.currentTime}
                      min={0}
                      max={this.state.totalDuration}
                      onChange={this.applyReaderTime}
                      onInput={this.setDragging}
                      />
               <audio id="podcast-reader"
                      onEnded={() => {markAsRead([id], () => {this.props.readOnEnd(id)})}}
                      onTimeUpdate={this.updateReaderTime}>
                  <source src={url}/>
                  <a href={url}> {url} </a>
               </audio>
               {Math.floor(this.props.currentEpisode.currentTime)}/{Math.floor(this.state.totalDuration)}
         </div>
      );
   }
}

enum ReaderAction {
   Play,
   Pause,
}

interface PlayPauseProperties {
   readerAction: ReaderAction;
   toggleReaderAction: () => void;
}

class PlayPause extends React.Component<PlayPauseProperties, {}> {
   render() {
      let img = "@{HomeR}/static/img/play-circle.svg";
      if (this.props.readerAction === ReaderAction.Play) {
         img = "@{HomeR}/static/img/pause-circle.svg";
      }

      return (
            <input className="action-button" onClick={this.props.toggleReaderAction} type="image" src={img} />
      );
   }
}

interface CollectionProperties {
   episodes: EpisodeData[];
   setCurrentEpisode: (EpisodeData) => void;
   currentEpisode: EpisodeData;
   removePlaylist: (string) => void;
}

class EpisodeCollection extends React.Component<CollectionProperties, {}> {
   render() {
      const episodes = [];

      this.props.episodes.forEach((episode) => {
         var nEpisode = episode;
         episodes.push(
            <Episode
               data = {nEpisode}
               removePlaylist = {this.props.removePlaylist}
               setCurrentEpisode = {this.props.setCurrentEpisode}
               isCurrentEpisode = {this.props.currentEpisode.id === episode.id}
            />
         );
      });

      return (
         <div className="container">
            {episodes}
         </div>
      );
   }
}


interface ActionBarProperties
extends ReaderProperties, PlayPauseProperties {}

class ActionBar extends React.Component<ActionBarProperties, {}> {
   render() {
      return (
         <div className="action-bar">
            <Reader readOnEnd={this.props.readOnEnd}
                    currentEpisode={this.props.currentEpisode}
                    setCurrentTime={this.props.setCurrentTime}
                    />
            <PlayPause toggleReaderAction={this.props.toggleReaderAction}
                       readerAction={this.props.readerAction}
                       />
         </div>
      );
   }
}

interface PodcastState {
   episodes: EpisodeData[];
   currentEpisode: EpisodeData;
   readerAction: ReaderAction;
}

var App = class extends React.Component<{}, PodcastState> {
   constructor(props) {
      super(props);
      var local = this;

      $.getJSON('@{PlaylistEpisodesR}', function (data) {
          var currentEpisode = null;

          data.forEach((episode) => {
             if (currentEpisode === null) {
                currentEpisode = episode;
             }
          });

          local.setState({episodes: data,
                          currentEpisode: currentEpisode,
                          readerAction: ReaderAction.Pause,
                          });

         var audio = $("#podcast-reader")[0] as HTMLAudioElement;
         audio.load();
         audio.currentTime = currentEpisode.currentTime;
      });

      this.state = { episodes: [],
                     currentEpisode: null,
                     readerAction: ReaderAction.Pause,
      }

      this.readOnEnd = this.readOnEnd.bind(this);
      this.removePlaylist = this.removePlaylist.bind(this);
      this.setCurrentTime = this.setCurrentTime.bind(this);
      this.setCurrentEpisode = this.setCurrentEpisode.bind(this);
      this.toggleReaderAction = this.toggleReaderAction.bind(this);
   }

   removePlaylist(id:string) {
      var episodes = this.state.episodes.filter((episode) => {return episode.id.localeCompare(id);});
      var local = this;
      var currentEpisode = this.state.currentEpisode;

      if (!currentEpisode.id.localeCompare(id)) {
        currentEpisode = null; 

        this.state.episodes.forEach((episode) => {
           if (currentEpisode === null && episode.id.localeCompare(id)) {
               currentEpisode = episode;
           }
        });
      }

      removePlaylist(id, () => {local.setState({episodes: episodes,
                                                currentEpisode: currentEpisode,
                                               });
      });
   }

   readOnEnd(id){
      this.setState({readerAction: ReaderAction.Pause});
      this.removePlaylist(id);
      setCurrentTime(this.state.currentEpisode.id, 0);
   }

   setCurrentEpisode(episode: EpisodeData) {
      setCurrentTime(this.state.currentEpisode.id, this.state.currentEpisode.currentTime);
      var eps = this.state.episodes;
      var i;

      for (i = 0; i < eps.length; i++) {
         if (eps[i].id === this.state.currentEpisode.id) {
            eps[i] = this.state.currentEpisode;
         }
      }

      this.setState({currentEpisode: episode,
                     readerAction: ReaderAction.Pause});
      var audio = $("#podcast-reader")[0] as HTMLAudioElement;
      audio.load();
      audio.currentTime = episode.currentTime;
   }

   setCurrentTime(id: string, currentTime: number) {
      var e = this.state.currentEpisode;
      e.currentTime = currentTime;
      this.setState({currentEpisode: e});
   }

   toggleReaderAction() {
      var audio = $("#podcast-reader")[0] as HTMLAudioElement;
      if(audio.paused){
         this.setState({readerAction: ReaderAction.Play});
         audio.play();
      } else {
         this.setState({readerAction: ReaderAction.Pause});
         audio.pause();
         setCurrentTime(this.state.currentEpisode.id, this.state.currentEpisode.currentTime);
      }
   }

   render() {
      return (
         <div>
            <AppLayout
               actionBar = {
                  <ActionBar
                     readOnEnd = {this.readOnEnd}
                     currentEpisode = {this.state.currentEpisode}
                     toggleReaderAction = {this.toggleReaderAction}
                     readerAction = {this.state.readerAction}
                     setCurrentTime = {this.setCurrentTime}
                     />
               }
               app = {
                  <EpisodeCollection
                     episodes = {this.state.episodes}
                     setCurrentEpisode = {this.setCurrentEpisode}
                     currentEpisode = {this.state.currentEpisode}
                     removePlaylist = {this.removePlaylist}
                     />
               }
               />
         </div>
      );
   }
}
