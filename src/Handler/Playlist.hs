{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}

module Handler.Playlist where

import BasicPrelude hiding (insert, map)
import Import

deletePlaylistR :: Handler ()
deletePlaylistR = do
    episodeKey <- lookupGetParam "id"
    case episodeKey of
        Just key -> do
            episode' <- runDB $ selectFirst [PlaylistEpisodeId ==. read key] []
            case episode' of
                Just episode -> runDB $ removeEpisodeFromPlaylist episode
                Nothing -> return ()
        Nothing -> return ()            
    return ()

postPlaylistR :: Handler ()
postPlaylistR = do
    episodeKey <- lookupGetParam "id"
    previous <- runDB $ selectFirst [PlaylistNext ==. Nothing] []
    case (episodeKey, previous) of
        (Just key', Just (Entity pId p)) -> do
            let key = read key'
            nkey <- runDB $ insert $ Playlist key ((playlistNumber p) + 1) (Just pId) Nothing
            runDB $ update pId [PlaylistNext =. Just nkey]
            runDB $ update key [EpisodePlaylist =. Just nkey]
        (Just key', Nothing) -> do
            let key = read key'
            nkey <- runDB $ insert $ Playlist key 0 Nothing Nothing
            runDB $ update key [EpisodePlaylist =. Just nkey]
        _ -> return ()

getPlaylistEpisodesR :: Handler Value
getPlaylistEpisodesR =  do
    playlist <- runDB $ selectList [] [Asc PlaylistNumber] 
    let episodes' = map (\(Entity _ p) -> playlistEpisodeId p) playlist
    episodes <- mapM (\e -> runDB $ getJustEntity e) episodes'
    returnJson $ map episodeData episodes
  where
    episodeData (Entity key episode) =
        let idEp = show key
            nameEp = episodeName episode
            url = episodeUrl episode
            isRead = episodeIsRead episode
            isInPlaylist = isNothing $ episodePlaylist episode
            currentTime = episodeCurrentTime episode
        in object $
           ["id" .= idEp, "url" .= url, "title" .= nameEp, "isRead" .= isRead, "isInPlaylist" .= isInPlaylist, "currentTime" .= currentTime]

getPlaylistR :: Handler Html
getPlaylistR =
    defaultLayout $ do
        setTitle "Playlist"
        $(widgetFile "playlist")
