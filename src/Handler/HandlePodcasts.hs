{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}

module Handler.HandlePodcasts where

import Import

import Control.Monad.Extra
import Control.Monad.Trans.Maybe
import Text.XML
import Text.XML.Cursor
import Yesod.Core.Json (returnJson)

import qualified Crypto.Saltine.Core.Utils as U
import qualified Data.ByteString.Char8 as BS (pack)

getPodcastInfos ::
       Maybe (String, String)
    -> Text
    -> Handler (Text, (Cursor -> [Text]) -> [Text], Text)
getPodcastInfos creds address = do
    htmlString <- getBody creds (unpack address)
    let doc = parseLBS_ def htmlString
    let cursor = fromDocument doc
    let title = getPodcastTitle cursor
    let logo = getPodcastLogo cursor
    let episodes = getPodcastEpisodes cursor
    return (title, episodes, logo)

addEpisodes :: Key Podcast -> ((Cursor -> [Text]) -> [Text]) -> Handler ()
addEpisodes idPod applyEpisode = do
    let titles = applyEpisode getEpisodeTitle
    let urls = applyEpisode getEpisodeUrl
    let guids = applyEpisode getEpisodeGuid
    let ds = zip3 titles urls guids
    mapM_
        (\(title, url, guid) ->
             runDB $
             addEpisode $
             Episode
                 (unpack guid)
                 (unpack title)
                 (unpack url)
                 idPod
                 Nothing
                 False
                 0)
        ds

updatePodcasts :: Handler ()
updatePodcasts = do
    podcasts <- runDB getAllPodcast
    let idurls =
            map
                (\(Entity idPod p) ->
                     (idPod, pack $ podcastUrl p, podcastCreds p))
                podcasts
    mapM_
        (\(idPod, url, creds') -> do
             app <- getYesod
             let kref = appSecret app
             k' <- readIORef kref
             creds <-
                 runMaybeT $ do
                     k <- liftMaybe k'
                     credsId <- liftMaybe creds'
                     PodCreds l nonce p' <- MaybeT $ runDB $ get credsId
                     (p, _) <- liftMaybe $ decrypt nonce k p'
                     return (l, show p)
             (_, episodes, _) <- getPodcastInfos creds url
             addEpisodes idPod episodes)
        idurls
    return ()

putUpdatePodcastsR :: Handler ()
putUpdatePodcastsR = do
    updatePodcasts
    return ()

handleAddPodcast :: Handler ()
handleAddPodcast = do
    addressM <- lookupGetParam "address"
    loginM <- lookupGetParam "login"
    passwordM <- lookupGetParam "password"
    case (addressM, loginM, passwordM) of
        (Just address, Just login, Just password) -> do
            app <- getYesod
            let kref = appSecret app
            k <- readIORef kref
            (creds, idCreds) <- getCreds k login password
            (title, episodes, logo) <- getPodcastInfos creds address
            liftIO $ putStrLn logo
            idPod <-
                runDB $
                addPodcast $
                Podcast (unpack title) (unpack address) (unpack logo) idCreds
            addEpisodes idPod episodes
        _ -> return ()
  where
    getCreds ::
           Maybe ByteString
        -> Text
        -> Text
        -> Handler (Maybe (String, String), Maybe PodCredsId)
    getCreds (Nothing) _ _ = return (Nothing, Nothing)
    getCreds _ "" "" = return (Nothing, Nothing)
    getCreds _ _ "" = return (Nothing, Nothing)
    getCreds _ "" _ = return (Nothing, Nothing)
    getCreds (Just k) l' p' = do
        let l = unpack l'
            p'' = unpack p'
        nonce <- liftIO $ U.randomByteString 12
        let p''' = encrypt nonce k $ BS.pack p''
        case p''' of
            Just (p, _) -> do
                idCreds <- runDB $ insert $ PodCreds l nonce p
                return (Just (l, p''), Just idCreds)
            Nothing -> return (Nothing, Nothing)

postAddPodcastR :: Handler ()
postAddPodcastR = do
    handleAddPodcast
    return ()

getPodcastsR :: Handler Value
getPodcastsR = do
    renderUrl <- getUrlRender
    podcasts' <- runDB getAllPodcast
    podcasts <- mapM podcastAndEpisode podcasts'
    returnJson $ map (nameAndAddress renderUrl) podcasts
  where
    nameAndAddress render (p, unread) =
        let pod = entityVal p
            name = podcastName pod
            logo = podcastLogo pod
            address = render $ PodcastR name
         in object $
            [ "name" .= name
            , "address" .= address
            , "logo" .= logo
            , "unread" .= unread
            ]
    podcastAndEpisode :: (Entity Podcast) -> Handler (Entity Podcast, Int)
    podcastAndEpisode p = do
        let key = entityKey p
        episodes <- runDB $ getEpisodesFromPodcastId key
        let unread = foldl' countUnread 0 episodes
        return (p, unread)
      where
        countUnread :: Int -> (Entity Episode) -> Int
        countUnread acc e =
            let episode = entityVal e
             in if episodeIsRead episode
                    then acc
                    else acc + 1
